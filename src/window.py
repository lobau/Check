# window.py
#
# Copyright 2023 Laurent Baumann
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from pathlib import Path
from os.path import exists
import sys
import re
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import GLib, Gio, Gtk, Adw, Gdk, Pango

from .todotxtio import todotxtio

import configparser

# @Gtk.Template(resource_path='/io/lobau/focus/window.ui')
class FocusFlatpakWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'FocusFlatpakWindow'

    label = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Writing prefs
        self.config = configparser.ConfigParser()
        self.config["DEFAULT"] = {"file-path": ""}
        self.config.read("config.ini")

        self.set_title("Focus")
        self.set_default_size(500, 700)
        self.current_todo_id = 0
        self.list_of_todos = {}
        self.is_dirty = False
        # self.is_new = False

        self.headerbar = Adw.HeaderBar()
        self.headerbar.get_style_context().add_class('flat')
        self.headerbar.set_show_end_title_buttons(True)
        self.set_titlebar(self.headerbar)

        self.menu_btn = Gtk.MenuButton(label="Menu")
        self.menu_btn.set_icon_name("open-menu-symbolic")
        self.headerbar.pack_start(self.menu_btn)

        ########
        ########

        menu_model = Gio.Menu()

        action_menu = Gio.Menu.new()
        # action_menu.append("New task", "win.new")
        action_menu.append("Open file", "win.open")

        system_menu = Gio.Menu.new()
        system_menu.append("About Focus", "win.about")
        # system_menu.append("Quit", "win.quit")

        menu_model.append_section(None, action_menu)
        menu_model.append_section(None, system_menu)

        # action_new = Gio.SimpleAction.new("new", None)
        # action_new.connect("activate", self.new_task)
        # self.add_action(action_new)

        action_open = Gio.SimpleAction.new("open", None)
        action_open.connect("activate", self.show_open_dialog)
        self.add_action(action_open)

        action_about = Gio.SimpleAction.new("about", None)
        action_about.connect("activate", self.on_about)
        self.add_action(action_about)

        # action_quit = Gio.SimpleAction.new("quit", None)
        # action_quit.connect("activate", self.quit)
        # self.add_action(action_quit)

        self.popover = Gtk.PopoverMenu()  # Create a new popover menu
        self.popover.set_menu_model(menu_model)

        self.menu_btn.set_popover(self.popover)

        ########
        ########

        self.open_dialog = Gtk.FileChooserNative.new(
                title="Choose a Todo.txt formatted file", parent=self, action=Gtk.FileChooserAction.OPEN)
        file_filter = Gtk.FileFilter()
        file_filter.set_name("Todo.txt formatted files")
        file_filter.add_suffix("txt")
        file_filter.add_suffix("todo")
        self.open_dialog.add_filter(file_filter)
        self.open_dialog.connect("response", self.open_response)

        self.scroll = Gtk.ScrolledWindow()

        self.grid = Gtk.Grid()
        self.grid.attach(self.scroll, 0, 0, 1, 1)

        separator = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        self.grid.attach(separator, 0, 1, 1, 1)

        self.input_container = Gtk.Box()
        self.input_container.set_margin_top(16)
        self.input_container.set_margin_bottom(16)
        self.input_container.set_margin_start(16)
        self.input_container.set_margin_end(16)

        # self.input_box = Gtk.ListBox()
        # self.input_box.get_style_context().add_class('boxed-list')
        # self.input_box.set_selection_mode(Gtk.SelectionMode.NONE)
        # self.input_box.set_hexpand(True)
        # self.input_container.append(self.input_box)

        # self.input_field = Adw.EntryRow()
        self.input_field = Gtk.Entry()
        # self.input_field.set_margin_top(10)
        # self.input_field.set_margin_bottom(10)
        # self.input_field.set_margin_start(10)
        # self.input_field.set_margin_end(10)

        # self.input_field.set_title("New task")
        self.input_field.set_hexpand(True)
        self.input_field.set_icon_from_icon_name(Gtk.EntryIconPosition.PRIMARY, "list-add-symbolic")
        self.input_field.set_placeholder_text("New task")
        self.input_field.connect("activate", self.create_task)
        self.input_container.append(self.input_field)

        self.grid.attach(self.input_container, 0, 2, 1, 1)
        self.grid.set_row_homogeneous(False)
        self.scroll.set_vexpand(True)

        self.set_child(self.grid)

        self.root = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.root.set_hexpand(True)
        self.root.set_vexpand(True)
        self.root.set_spacing(16)
        self.root.set_margin_top(16)
        self.root.set_margin_bottom(16)
        self.root.set_margin_start(16)
        self.root.set_margin_end(16)
        self.scroll.set_child(self.root)

        self.get_path()

        # self.file_path = ""
        # self.is_empty = True

        if exists(self.file_path):
            self.is_empty = False
            # self.set_toolbar()
            self.vbox = Gtk.ListBox()
            self.root.append(self.vbox)
            self.load_file()
        else:
            self.is_empty = True
            self.welcome = Adw.StatusPage()
            self.welcome.set_hexpand(True)
            self.welcome.set_vexpand(True)
            self.welcome.set_title("Focus")
            self.welcome.set_description("A simple todo.txt manager")
            self.welcome.set_icon_name("io.lobau.Focus-symbolic")
            self.root.append(self.welcome)

            choose_file = Gtk.Button(label="Choose a file")
            choose_file.get_style_context().add_class('suggested-action')
            choose_file.get_style_context().add_class('pill')
            choose_file.set_halign(Gtk.Align.CENTER)
            self.welcome.set_child(choose_file)
            choose_file.connect("clicked", self.show_open_dialog)

        self.edit_window = Gtk.Window()
        self.edit_window.set_modal(True)
        self.edit_window.set_transient_for(self)
        self.edit_window.set_title("")
        self.edit_window.set_default_size(400, 300)

        headerbar = Adw.HeaderBar()
        headerbar.get_style_context().add_class('flat')
        headerbar.set_show_end_title_buttons(False)
        self.edit_window.set_titlebar(headerbar)

        cancel_btn = Gtk.Button(label="Cancel")
        cancel_btn.connect("clicked", self.close_modal)
        headerbar.pack_start(cancel_btn)

        # delete_btn = Gtk.Button(label="Delete")
        # delete_btn.set_icon_name("user-trash-symbolic")
        # delete_btn.get_style_context().add_class('destructive-action')
        # delete_btn.connect("clicked", self.delete_task)
        # headerbar.pack_start(delete_btn)

        done_btn = Gtk.Button(label="Done")
        done_btn.get_style_context().add_class('suggested-action')
        done_btn.connect("clicked", self.commit_edit)
        headerbar.pack_end(done_btn)

        edit_root = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        edit_root.set_hexpand(True)
        edit_root.set_vexpand(True)
        edit_root.set_spacing(8)
        edit_root.set_margin_top(8)
        edit_root.set_margin_bottom(8)
        edit_root.set_margin_start(8)
        edit_root.set_margin_end(8)
        edit_root.get_style_context().add_class('boxed-list')
        self.edit_window.set_child(edit_root)

        self.edit_field = Gtk.Entry()
        edit_root.append(self.edit_field)

    # def open_popover(self, button):
    #     self.popover.set_position(Gtk.PositionType.BOTTOM)
    #     self.popover.show_all()

    def get_path(self):
        self.file_path = self.config.get("DEFAULT", "file-path")

    def save_path(self):
        self.config.set("DEFAULT", "file-path", self.file_path)
        with open("config.ini", "w") as configfile:
            self.config.write(configfile)

    def toggle_complete(self, button, todo):
        self.current_todo_id = todo.idx
        self.list_of_todos[self.current_todo_id].completed = button.get_active()
        self.is_dirty = True
        self.render()

    def create_task(self, button):
        text = self.input_field.get_text()
        new_todo = todotxtio.from_string(todotxtio, text)
        self.list_of_todos.append(new_todo[0])
        self.is_dirty = True

        # Clear the field
        self.input_field.set_text("")

        self.render()

    def commit_edit(self, button):
        text = self.edit_field.get_text()
        new_todo = todotxtio.from_string(todotxtio, text)

        self.list_of_todos[self.current_todo_id] = new_todo[0]
        self.is_dirty = True

        self.edit_window.hide()
        self.render()

    def close_modal(self, button):
        self.edit_window.hide();

    def edit_task(self, button, todo):
        self.current_todo_id = todo.idx
        self.edit_field.set_text(str(todo))
        self.edit_field.grab_focus()
        self.edit_window.present()

    def open_link(self, button, todo):
        Gio.AppInfo.launch_default_for_uri(str(todo.link), None)

    def delete_task(self, button, todo):
        del self.list_of_todos[todo.idx]

        self.is_dirty = True
        self.edit_window.hide()
        self.render()

    def render(self):
        if self.is_empty:
            self.root.remove(self.welcome)
            self.is_empty = False
        else:
            self.root.remove(self.vbox)

        self.vbox = Gtk.ListBox()
        self.vbox.get_style_context().add_class('boxed-list')
        self.vbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.root.append(self.vbox)

        idx = 0;

        for todo in self.list_of_todos:

            todo.idx = idx
            idx += 1

            # Find all the links
            link_regex = r'http[s]?://(?:[a-zA-Z0-9$-_@.&+]|[!*\(\),])+(?:#[a-zA-Z0-9$-_@.&+]+)?'
            links = re.findall(link_regex, todo.text)

            todo_text = re.sub(link_regex, '…', todo.text)
            # if len(todo_text) == 0:
            #     todo_text = '🔗 Link'

            icon = Gtk.CheckButton.new()
            icon.get_style_context().add_class('selection-mode')

            if todo.completed:
                icon.set_active(todo.completed)

            #######
            ####### TEST ROW
            test_row = Adw.ExpanderRow()
            test_row.set_title(todo_text)
            self.vbox.append(test_row)
            test_row.add_prefix(icon)

            action_row = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
            action_row.set_spacing(8)
            action_row.set_margin_top(12)
            action_row.set_margin_bottom(12)
            action_row.set_margin_start(12)
            action_row.set_margin_end(12)
            test_row.add_row(action_row)
            # todo_box.(action_row)

            button_edit_task = Gtk.Button.new_with_label(label='Edit')
            button_edit_task.set_icon_name(icon_name='document-edit-symbolic')
            # button_edit_task.get_style_context().add_class('flat')
            button_edit_task.get_style_context().add_class('suggested-action')
            button_edit_task.get_style_context().add_class('circular')
            # button_edit_task.set_valign(align=Gtk.Align.CENTER)
            action_row.append(button_edit_task)

            delete_btn = Gtk.Button(label="Delete")
            delete_btn.set_icon_name("user-trash-symbolic")
            delete_btn.get_style_context().add_class('destructive-action')
            delete_btn.get_style_context().add_class('circular')
            action_row.append(delete_btn)

            if len(links) > 0:
                # test_row.set_subtitle("🔗 Link")
                todo.link = links[0]

                link_button = Gtk.Button(label=todo.link)
                # link_button.set_hexpand(True)
                link_button.set_vexpand(True)
                label = link_button.get_child()
                label.set_property("ellipsize", Pango.EllipsizeMode.END)  # set the ellipsize mode to end
                link_button.connect("clicked", self.open_link, todo)
                action_row.append(link_button)

            icon.connect("toggled", self.toggle_complete, todo)
            button_edit_task.connect("clicked", self.edit_task, todo)
            delete_btn.connect("clicked", self.delete_task, todo)

            # self.vbox.append(task_row)

        if self.is_dirty:
            self.is_dirty = False
            todotxtio.to_file(todotxtio, self.file_path, self.list_of_todos)

    def show_open_dialog(self, button, menu):
        self.open_dialog.show()
        self.popover.popdown()

    def open_response(self, dialog, response):
        if response == Gtk.ResponseType.ACCEPT:
            file = dialog.get_file()
            self.file_path = file.get_path()
            self.load_file()

    def on_file_changed(self, monitor, f1, f2, evt):
        print("Changed:", f1, f2, evt)
        self.load_file()

    def load_file(self):
        filename = os.path.basename(self.file_path)
        self.set_title(filename)
        self.list_of_todos = todotxtio.from_file(todotxtio, self.file_path)

        gio_file = Gio.File.new_for_path(self.file_path)
        self.gio_file_monitor = gio_file.monitor_file(Gio.FileMonitorFlags.NONE, None)
        self.gio_file_monitor.connect("changed", self.on_file_changed)

        self.save_path()
        self.is_dirty = False
        # self.is_welcome = False

        self.render()

    # def set_toolbar(self):
        # self.open_btn = Gtk.Button(label="Open")
        # self.open_btn.set_icon_name("document-open-symbolic")
        # self.open_btn.connect("clicked", self.show_open_dialog)
        # self.headerbar.pack_start(self.open_btn)

        # self.add_btn = Gtk.Button(label="Add")
        # self.add_btn.set_icon_name("list-add")
        # self.headerbar.pack_start(self.add_btn)
        # self.add_btn.connect("clicked", self.new_task)

    def on_about(self, *args):
        about = Adw.AboutWindow(
            transient_for=self,
            application_name='Focus',
            application_icon='io.lobau.Focus',
            developer_name='Laurent Baumann',
            license_type=Gtk.License.GPL_3_0,
            website='https://gitlab.com/lobau/focus/',
            issue_url='https://gitlab.com/lobau/focus/-/issues/new',
            version='0.1',
            developers=[
                'Laurent Baumann https://lobau.io'
            ],
            designers=[
                'Laurent Baumann https://lobau.io'
            ],
            translator_credits=_("translator-credits"),
            copyright='© Laurent Baumann'
        )
        about.add_credit_section(
            _('Contributors'),
            [
                'Elin Wong'
            ]
        )
        self.popover.popdown()
        about.present()

    # @staticmethod
    # def quit(self, dummy):
    #     Gtk.main_quit()

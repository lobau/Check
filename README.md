# Focus

A simple todo.txt based task manager.
Learn more how to format the todo files at [todotxt.org](http://todotxt.org/)

To build, open the folder in Gnome Builder, and press play!
